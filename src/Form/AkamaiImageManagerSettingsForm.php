<?php

namespace Drupal\akamai_image_manager\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Provides the Akamai Image Manager settings form.
 *
 * @package Drupal\akamai_image_manager\Form
 */
class AkamaiImageManagerSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'akamai_image_manager_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['akamai_image_manager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('akamai_image_manager.settings');

    $form['akamai_image_manager']['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#default_value' => $config->get('domain'),
      '#description' => $this->t('Akamai Image Manager domain name.'),
      '#required' => TRUE,
    ];

    $form['akamai_image_manager']['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $config->get('secret_key'),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="unsafe"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="unsafe"]' => ['checked' => FALSE],
        ],
      ],
      '#description' => $this->t('The secret key of AIM which encrypts all calls.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('akamai_image_manager.settings');
    $config->delete();

    foreach (Element::children($form['akamai_image_manager']) as $key) {
      $config->set($key, $form_state->getValue($form['akamai_image_manager'][$key]['#parents']));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
