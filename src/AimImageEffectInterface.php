<?php

namespace Drupal\akamai_image_manager;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Interface AimImageEffectInterface.
 *
 * @package Drupal\akamai_image_manager
 */
interface AimImageEffectInterface extends PluginFormInterface, ConfigurableInterface {

  /**
   * Returns the weight of the image effect.
   *
   * @return int
   *   Either the integer weight of the image effect, or an empty string.
   */
  public function getWeight();

  /**
   * Sets the weight for this image effect.
   *
   * @param int $weight
   *   The weight for this image effect.
   *
   * @return $this
   */
  public function setWeight(int $weight);

  /**
   * Sets the status of the image effect.
   *
   * @param bool $status
   *   The status for this effect.
   *
   * @return $this
   *   Returns the status of the this effect.
   */
  public function setStatus(bool $status);

  /**
   * Returns the status of the image effect.
   *
   * @return bool
   *   Returns the status of the AIM effect.
   */
  public function getStatus();

  /**
   * Returns URL params of effect.
   *
   * @param string $base_uri
   *   Base file uri.
   *
   * @return string
   *   Returns settings string.
   */
  public function getUrlParams(string $base_uri);

}
