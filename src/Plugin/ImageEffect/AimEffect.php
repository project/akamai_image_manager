<?php

namespace Drupal\akamai_image_manager\Plugin\ImageEffect;

use Drupal\akamai_image_manager\AimImageEffectPluginManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Akamai image manager effects.
 *
 * @ImageEffect(
 *   id = "aim_effects",
 *   label = @Translation("Akamai Image Manager Effects"),
 *   description = @Translation("Use Akamai image manager effects.")
 * )
 */
class AimEffect extends ConfigurableImageEffectBase {

  /**
   * AIM image plugin manager.
   *
   * @var \Drupal\akamai_image_manager\AimImageEffectPluginManager
   */
  protected $aimImagePluginManager;

  /**
   * Array of effect plugins.
   *
   * @var array
   */
  protected $imageEffects;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, AimImageEffectPluginManager $aim_image_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);

    $this->aimImagePluginManager = $aim_image_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('image'),
      $container->get('plugin.manager.aim_image_effect')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function transformDimensions(array &$dimensions, $uri) {
    if ($effect_config = $this->configuration['image_resize_effect']) {
      if ($effect_config['status']) {
        $dimensions['width'] = $effect_config['data']['width'];
        $dimensions['height'] = $effect_config['data']['height'];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $definitions = $this->aimImagePluginManager->getDefinitions();
    $form['effects'] = [];
    /** @var \Drupal\akamai_image_manager\AimImageEffectInterface $effect_plugin */
    foreach ($definitions as $effect_id => $effect_plugin) {
      $effect_config = isset($this->configuration[$effect_id]) ? $this->configuration[$effect_id] : [];
      $effect_plugin = $this->aimImagePluginManager->createInstance($effect_id, $effect_config);

      $form['effects'][$effect_id] = [
        '#type' => 'fieldset',
        '#title' => $effect_plugin->getPluginDefinition()['label'],
        '#weight' => $effect_plugin->getStatus() ? $effect_plugin->getWeight() : 99,
      ];
      $form['effects'][$effect_id]['status'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable'),
        '#description' => $effect_plugin->getPluginDefinition()['description'],
        '#default_value' => $effect_plugin->getStatus() ?: FALSE,
      ];
      $form['effects'][$effect_id]['settings'] = [
        '#type' => 'container',
        '#states' => [
          'visible' => [
            ':input[name="data[effects][' . $effect_id . '][status]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['effects'][$effect_id]['settings']['weight'] = [
        '#type' => 'select',
        '#title' => $this->t('weight'),
        '#options' => range(0, count($definitions)),
        '#default_value' => $effect_plugin->getWeight() ?: 0,
      ];

      $form['effects'][$effect_id]['settings']['data'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Settings'),
      ];
      $effect_plugin_form_state = SubformState::createForSubform($form['effects'][$effect_id]['settings']['data'], $form, $form_state);

      $form['effects'][$effect_id]['settings']['data'] = $effect_plugin->buildConfigurationForm($form['effects'][$effect_id]['settings']['data'], $effect_plugin_form_state);
      $this->imageEffects[$effect_id] = $effect_plugin;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\akamai_image_manager\AimImageEffectInterface $effect_plugin */
    $effect_config = [];
    foreach ($this->imageEffects as $effect_id => $effect_plugin) {
      $effect_plugin_form_state = SubformState::createForSubform($form['effects'][$effect_id]['settings']['data'], $form, $form_state);
      $effect_plugin->submitConfigurationForm($form['effects'][$effect_id]['settings']['data'], $effect_plugin_form_state);
      $effect_plugin->setStatus($form_state->getValue('effects')[$effect_id]['status']);
      $effect_plugin->setWeight($form_state->getValue('effects')[$effect_id]['settings']['weight']);
      $effect_config[$effect_id] = $effect_plugin->getConfiguration();
    }
    $this->configuration = $effect_config;
  }

  /**
   * Get the Akamai image manager URL for the specified transformations.
   *
   * @param array $configuration
   *   The Akamai image manager effects configuration.
   * @param string $image_uri
   *   The original uri of the image that needs to be transformed.
   * @param string $base_uri
   *   The base uri of the image file.
   *
   * @return string
   *   The formatted Akamai image manager URL.
   */
  public static function getUrl(array $configuration, string $image_uri, string $base_uri): string {
    $aim_effects_params = [];
    /** @var \Drupal\akamai_image_manager\AimImageEffectPluginManager $aim_plugin_manager */
    $aim_plugin_manager = \Drupal::service('plugin.manager.aim_image_effect');
    $definitions = $aim_plugin_manager->getDefinitions();
    $plugin_instances = [];
    /** @var \Drupal\akamai_image_manager\AimImageEffectInterface $effect_plugin */
    foreach ($definitions as $effect_id => $plugin) {
      if (isset($configuration[$effect_id])) {
        $effect_plugin = $aim_plugin_manager->createInstance($effect_id, $configuration[$effect_id]);
        if ($effect_plugin->getStatus()) {
          $plugin_instances[$effect_plugin->getWeight()] = $effect_plugin;
        }
      }
    }
    ksort($plugin_instances);
    foreach ($plugin_instances as $weight => $effect_plugin) {
      if ($effect_plugin->getUrlParams($base_uri) !== '') {
        $aim_effects_params[$weight] = $effect_plugin->getUrlParams($base_uri);
      }
    }

    $aim_url_params = '?im=' . \implode(';', $aim_effects_params);

    $settings = \Drupal::config('akamai_image_manager.settings');
    $secret_key = $settings->get('secret_key');
    $hash = hash('sha256', $secret_key . $image_uri . $aim_url_params);
    $domain = $settings->get('domain');
    $url = 'https://' . $domain . $image_uri . $aim_url_params . '&hash=' . $hash;

    return $url;
  }

}
