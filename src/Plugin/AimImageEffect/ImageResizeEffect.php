<?php

namespace Drupal\akamai_image_manager\Plugin\AimImageEffect;

use Drupal\akamai_image_manager\AimImageEffectBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides scale effect.
 *
 * @AimImageEffect(
 *   id = "image_resize_effect",
 *   label = @Translation("Image resize effect"),
 *   description = @Translation("Use Akamai Image Manager resize effect.")
 * )
 */
class ImageResizeEffect extends AimImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $settings['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#min' => 0,
      '#default_value' => $this->configuration['width'],
    ];

    $settings['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#min' => 0,
      '#default_value' => $this->configuration['height'],
    ];

    $settings['aspect'] = [
      '#type' => 'select',
      '#title' => $this->t('Aspect'),
      '#options' => ['ignore' => 'ignore', 'fit' => 'fit', 'fill' => 'fill'],
      '#default_value' => $this->configuration['aspect'] ?: 'ignore',
    ];

    $form['settings'] = $settings;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration = $form_state->getValue('settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlParams($base_uri): string {
    $configuration = $this->configuration;
    $resize_effect_params = [];

    foreach ($configuration as $id => $value) {
      if ($value && $value !== '') {
        $resize_effect_params[$id] = $id . '=' . $value;
      }
    }
    $resize_effect = 'Resize,' . \implode(',', $resize_effect_params);

    return $resize_effect;
  }

}
