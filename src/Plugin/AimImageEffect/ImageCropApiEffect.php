<?php

namespace Drupal\akamai_image_manager\Plugin\AimImageEffect;

use Drupal\akamai_image_manager\AimImageEffectBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\crop\Entity\Crop;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides scale effect.
 *
 * @AimImageEffect(
 *   id = "image_crop_api_effect",
 *   label = @Translation("Image crop api effect"),
 *   description = @Translation("Use Crop API with Akamai Image Manager crop effect.")
 * )
 */
class ImageCropApiEffect extends AimImageEffectBase {

  /**
   * Loger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Crop type entity storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $typeStorage = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    if ($entityTypeManager->getDefinition('crop_type', FALSE)) {
      $this->typeStorage = $entityTypeManager->getStorage('crop_type');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('akamai_image_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'crop_type' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $settings = [];
    if ($this->typeStorage) {
      $options = [];
      foreach ($this->typeStorage->loadMultiple() as $type) {
        $options[$type->id()] = $type->label();
      }

      $settings['crop_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Crop type'),
        '#default_value' => $this->configuration['crop_type'],
        '#options' => $options,
        '#description' => $this->t('Crop type to be used for the image style.'),
      ];
    }
    else {
      $form['message'] = [
        '#type' => 'item',
        '#markup' => $this->t('This crop effect requires <a href="@module">Crop API module</a>.', ['@module' => 'https://www.drupal.org/project/crop']),
      ];
    }

    $form['settings'] = $settings;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration = $form_state->getValue('settings') ?: $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlParams($base_uri): string {
    $crop_effect = '';
    if ($this->typeStorage) {
      if (empty($this->configuration['crop_type']) || !$this->typeStorage->load($this->configuration['crop_type'])) {
        $this->logger->error('Manual image crop failed due to misconfigured crop type on %path.', ['%path' => $base_uri]);
      }

      if ($crop = $this->getCrop($base_uri)) {
        $anchor = $crop->anchor();
        $size = $crop->size();

        $crop_effect = 'Crop,rect=(' . $anchor['x'] . ',' . $anchor['y'] . ',' . $size['width'] . ',' . $size['height'] . ')';
      }
    }

    return $crop_effect;
  }

  /**
   * Gets crop entity for the image.
   *
   * @param string $uri
   *   Image object.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\crop\CropInterface|false
   *   Crop entity or FALSE if crop doesn't exist.
   */
  protected function getCrop(string $uri) {
    if (!isset($this->crop)) {
      $this->crop = FALSE;
      if ($crop = Crop::findCrop($uri, $this->configuration['crop_type'])) {
        $this->crop = $crop;
      }
    }

    return $this->crop;
  }

}
