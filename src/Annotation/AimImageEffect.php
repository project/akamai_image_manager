<?php

namespace Drupal\akamai_image_manager\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a AimImageEffect plugin annotation.
 *
 * @see \Drupal\akamai_image_manager\AimImageEffectPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class AimImageEffect extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the image effect.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief description of the image effect.
   *
   * This will be shown when adding or configuring this image effect.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

}
